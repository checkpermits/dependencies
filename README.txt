The dependency graph project is a real life code sample which I used to parse pseudo-language converting tokens into a SQL.
This dependency graph operates with any type of dependency objects/strings and allows to build a parsing graph of dependencies of a given object in a graph.
I choose to attach separate files instead of zipped code directory, because sometimes mail servers do not accept zip files as attachments.

The project contains folowing files:

1. com.companyName.graphs.DirectedGraph.java
      This class implements the directed graph infrastructure.

2. com.companyName.graphs.exceptions.DependencyException.java and com.companyName.graphs.exceptions.SortElementsException.java
      Those classes implement exceptionswhich could be thrown when constructing a directed graph.

3. com.companyName.graphs.tests.GraphTest.java
      Test cases for testing directed graph functionalitu



In order to run the project you need

1. jdk installed
2. junit JAR added to the project. Could be downloaded at: http://search.maven.org/#search|gav|1|g:"junit" AND a:"junit"
3. Place the file DirectedGraph.java into com/companyName/graph folder (create it if missing).
4. Place the DependencyException.java and SortElementsException.java into com/companyName/graph/exceptions folder.
5. Place the GraphTest.java into com/companyName/graph/tests folder.

Add junit jar, compile the project and run junits text case : GraphTest.