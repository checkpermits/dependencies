package com.companyName.graphs.tests;

import java.util.List;

import com.companyName.graphs.DirectedGraph;
import com.companyName.graphs.exceptions.DependencyException;

import junit.framework.TestCase;
/**
 * This test case validates different scenarios of constructing and manipulating a directed graph.
 */
public class GraphTest extends TestCase
{
    /**
     * This method tests valid scenario of adding elements to a graph : A -> B -> C -> D
     */
    public void testValidGraph()
    {
        try
        {
            DirectedGraph graph = new DirectedGraph();

            graph.addDependency("A", "B");
            graph.addDependency("B", "C");
            graph.addDependency("C", "D");

            List<Object> sorted = graph.sortElements();
            
            assertTrue("Wrong number of sorted elements, must be 4, but is " + sorted.size(), sorted.size() == 4);
            
            assertTrue("First sorted element is invalid", sorted.get(0).equals("D"));
            assertTrue("Second sorted element is invalid", sorted.get(1).equals("C"));
            assertTrue("Third sorted element is invalid", sorted.get(2).equals("B"));
            assertTrue("Forth sorted element is invalid", sorted.get(3).equals("A"));
        } 
        catch (DependencyException e)
        {
            fail(e.getMessage());
        }
    }
    
    
    /**
     * This method tests cyclic loops in the graph
     */
    public void testCyclicLoop()
    {
        try
        {
            DirectedGraph graph = new DirectedGraph();

            graph.addDependency("A", "B");
            graph.addDependency("B", "C");
            graph.addDependency("C", "A");

            graph.sortElements();
            
            fail("Invalid Graph can not contain cyclic loops");
        } 
        catch (DependencyException e)
        {
            // we should get an exception, because a graph contains cyclic loops
            assertTrue(e.getMessage() != null);
        }
    }
}
