package com.companyName.graphs.exceptions;

import java.util.*;


/**
 * This exception signals a problem when sorting elements in a directed graph, such as 
 * encountering circular dependency or accessing non-existing node 
 */
public class SortElementsException extends DependencyException 
{
    /**
     * Collection of currently sorted elements used for debugging
     */
    private Collection<Object> m_sortedElements = null;
    
    public SortElementsException(String message, List<Object> sortedElements) {
        super(message);
        m_sortedElements = sortedElements;
    }
    
    /**
     * This method returns the current state of sorted elements in the graph.
     * @return currently sorted elements
     */
    public Collection<Object> getSortedElements() {
        return m_sortedElements;
    }
}

