package com.companyName.graphs.exceptions;

/**
 * Dependency exception is an exception thrown when constructing directed graph.
 * Some of the examples when it is thrown: access to a non-existing node or removing non-existing node in directed graph
 */
public class DependencyException extends Exception
{
    /**
     * Constructs dependency exception based on the provided message
     * @param message error message
     */
    public DependencyException(String message)
    {
        super(message);
    }
}
