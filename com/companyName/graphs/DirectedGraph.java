package com.companyName.graphs;

import com.companyName.graphs.exceptions.*;
import java.util.*;

/**
 * This class encapsulates in-memory representation of directed graph.
 * The graph is built using pairs of any generic type objects having parent to child relationship.
 * For example, following set of strings constitutes a directed graph with circular dependencies:
 * {"A" -> "B", "B" -> "C", "B" -> "D", "C" -> "A"}.
 * 
 * The algorithm to print a dependency graph is as following:
 * 1. Create a directed graph from pairs of parent -> child nodes.
 * 2. Found leaves in the graph and update their indexes (those indices indicate their sorting order)
 * 3. Remove leaves from the graph
 * 4. Repeat 2-3 until the all nodes have sorted indices or find circular dependency
 * 
 */

public class DirectedGraph
{
    /**
     * Map of objects to their internal node presentation
     */
    private Map<Object, Node> m_nodes = new HashMap<Object, Node>();
    // result of the dependency graph resolution - sorted elements, processing
    // order is from left to right.
    /**
     *  List of sorted elements.
     */
    private List<Object> m_sortedElements = new ArrayList<Object>();
    /**
     * Boolean indication whether the graph was modified, in this case recalculate dependencies.
     */
    private boolean m_recalculateDependencies = true;
    
    // string constants
    private static final String ARROW = " -> " ;
    private static final String SEPARATOR_LINE = "------------------------------------------\n";
    private static final String NEW_LINE = "\n";
    private static final String TOTAL = "Total elements: ";
    private static final String NODE = "Node: ";
    private static final String PARENTS = "Parents: ";
    private static final String CHILDREN = "Children: ";
    private static final String NULL_CHILD = "Child can not be null";
    
    /**
     * Adds dependency to the graph
     * 
     * @param parent
     *            parent key
     * @param child
     *            child key
     * @throws DependencyException
     */
    public void addDependency(Object parent, Object child) throws DependencyException
    {
        Node parentNode = createNodeIfNeeded(parent);
        Node childNode = createNodeIfNeeded(child);

        // add cross-node dependency if needed   
        if(parent != null && child != null && parent.equals(child) == false)
        {
            parentNode.addChild(childNode);
            childNode.addParent(parentNode);
        }

        // graph was modified - recalculate the elements
        if(parent != null || child != null)
           m_recalculateDependencies = true;
    }
    
    /**
     * Collects children for a node with a given key
     * @param key - node key 
     * @return collection of children for a given node
     * @throws DependencyException
     */
    public Collection<Object> getChildren(Object key) throws DependencyException { 
        Set<Object> children = new HashSet<Object>();
        Node curNode = m_nodes.get(key);
        if (curNode == null ) {   
            throw new DependencyException("Invalid dependent's code '" + key + "' does not exist");
        }
        
        Set<Node> nodes = curNode.getChildren();
        if (nodes == null) 
            return children;
        Iterator<Node> itt = nodes.iterator();
        while (itt.hasNext()) {
            Node tmp = itt.next();
            children.add(tmp.getKey());
        }
        return children;
    }
    
    /**
     * Returns all dependent nodes of the graph
     * @param key node's key
     * @return collection of all dependent nodes on the node, associated with the key
     * @throws DependencyException
     */
    public Collection<Object> getDependents(Object key) throws DependencyException{
        List<Object> traversePath = new ArrayList<Object>();
        Set<Object> dependents = new HashSet<Object>();
        Node root = m_nodes.get(key);
        
        if (root == null ){   
            throw new DependencyException("Invalid dependent's code '" + key + "' does not exist");
        }
           
        traverse(root, traversePath, dependents);
        
        return dependents;
    }
    
    /**
     * This method traverses a path in the graph, while storing dependencies of the root node and validation no circular dependencies are encountered.
     * @param root
     * @param traversePath
     * @param dependents
     * @throws DependencyException
     */
    private void traverse(Node root, List<Object> traversePath, Set<Object> dependents) throws DependencyException{
        
        if (traversePath.contains(root) == true )
        {
            throw new DependencyException("Measure expressions contain cyclical dependency : " + getDependencyLoop(traversePath));
            
        }
        traversePath.add(root);
        dependents.add(root.getKey());
    
        if (root.getChildren()!= null) {
           Iterator<Node> children = root.getChildren().iterator();
           Node temp ;
           while (children.hasNext()){
               temp = children.next();
               
               traverse(temp, traversePath, dependents);
               traversePath.remove(temp);
           }
         }
    }
    
    /**
     * This method formats a circular dependency path for pretty printing.
     * @param traversePath traverse path that contains a cicular dependency
     * 
     * @return string with a pretty formatted dependency loop
     */
    private String getDependencyLoop(List<Object> traversePath){
        StringBuffer buffer  =  new StringBuffer();
        int i, size = traversePath != null ? traversePath.size() : 0 ; 
        
        for (i =0 ; i < size ; i++){
            if (i > 0)
                buffer.append(ARROW);
            
            buffer.append(traversePath.get(i));
        }
        
        if(size > 0)
            buffer.append(ARROW).append(traversePath.get(0));
            
        
        return buffer.toString();
    }
    
    /**
     * This method removed dependent nodes from the parent node, when we create a dependency tree.
     * @param parents
     * @param child
     * @return
     * @throws DependencyException
     */
    private boolean removeDependency(Set<Node> parents, Object child) throws DependencyException
    {
        boolean result = true;
        
        if(parents != null)
        {
            Iterator<Node> iterator = parents.iterator();
            List<Node> nodes2Remove = new ArrayList<Node>();
            Node node;
            
            while(iterator.hasNext())
            {
                node = iterator.next();
                nodes2Remove.add(node);
            }
            
            int i, size = nodes2Remove.size();
            for(i=0; i<size; i++)
            {
                result = result && removeDependency(nodes2Remove.get(i).getKey(), child);
            }
            
            return result;
        }
        else
        {
            return false;
        }
    }

    
    /**
     * Removes dependency from the graph
     * 
     * @param parent
     *            parent key
     * @param child
     *            child key
     * @return true if dependency was removed, false when such dependencyt does
     *         not exist
     * @throws DependencyException
     */
    public boolean removeDependency(Object parent, Object child) throws DependencyException
    {
        Node parentNode = parent != null ? m_nodes.get(parent) : null;
        Node childNode = child != null ? m_nodes.get(child) : null;
        boolean result = true;

        // remove child from parent node
        if (parentNode != null && childNode != null)
        {
            result = result && parentNode.removeChild(childNode);
        }

        // remove parent from child node
        result = result && childNode.removeParent(parentNode);

        m_recalculateDependencies = true;

        return result;
    }


    /**
     * This is the core method which sorts elements of the directed graph.
     * Review the algorithm description at the class header.
     * @return the elements, sorted by dependency order
     * @throws DependencyException
     *             if there is a cyclic dependency (loop) in the graph
     */
    public List<Object> sortElements() throws SortElementsException
    {
        try {
            if (m_recalculateDependencies) {
                m_sortedElements.clear();
                int i, size, processedElements = 0;
                Map<Object, Node> nodes2process = m_nodes; //cloneGraph();
                Iterator<Node> iterator;
                Node temp;
                int leafsCountPerIteration = 0;
                List<Object> nodes2remove = new ArrayList<Object>();

                // loop over all unprocessed nodes
                while (processedElements < nodes2process.size()) 
                {
                    leafsCountPerIteration = 0;
                    nodes2remove.clear();
                    iterator = nodes2process.values().iterator();
                    while (iterator.hasNext()) 
                    {
                        temp = iterator.next();
                        // check if there is a leaf
                        if (temp.getChildrenSize() == 0) 
                        {
                            leafsCountPerIteration++;
                            m_sortedElements.add(temp.getKey());
                            nodes2remove.add(temp.getKey());
                            removeDependency(temp.getParents(), temp.getKey());
                        }
                    }

                    // remove nodes
                    size = nodes2remove.size();
                    for (i = 0; i < size; i++)
                        nodes2process.remove(nodes2remove.get(i));
                    // each iteration must be at least one leaf removed, if no leaves found inside non-empty graph , means there are loops
                    if (leafsCountPerIteration == 0)
                        throw new DependencyException("Graph has circular dependencies.");
                }
                // this assignment must be done by the end of the method, since
                // removeDependency call affects this
                m_recalculateDependencies = false;
            }
        } catch (DependencyException e) {
            throw new SortElementsException(e.getMessage(), m_sortedElements);
        }

        return m_sortedElements;
    }

    /**
     * This method creates node for a new element, if an element exists in the graph, node is not created.
     * @param key
     * @return
     * @throws DependencyException
     */
    private Node createNodeIfNeeded(Object key) throws DependencyException
    {
        if(key == null)
            return null;
        
        Node node = key != null ? m_nodes.get(key) : null;
        if (node == null)
        {
            node = new Node(key);
        }
        m_nodes.put(key, node);

        return node;
    }
    
    /**
     * @return a printable state of the graph
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator<Node> iterator = m_nodes.values().iterator();
        Node node;
        
        
        buffer.append(TOTAL).append(m_nodes.size()).append(NEW_LINE);
        
        while(iterator.hasNext()) 
        {
            node = iterator.next();
            buffer.append(NODE).append(node.getKey()).append(NEW_LINE);
            buffer.append(PARENTS).append(node.getParents()).append(NEW_LINE);
            buffer.append(CHILDREN).append(node.getChildren()).append(NEW_LINE);
            buffer.append(SEPARATOR_LINE);
        }
        
        return buffer.toString();
    }

    /**
     * A node object to represent an external object in the graph
     * All the methods are self-explanatory
     */
    class Node
    {
        private Object m_key = null;
        private Set<Node> m_parents = null;
        private Set<Node> m_children = null;

        public Node(Object key) throws DependencyException
        {
            if (key == null)
                throw new DependencyException(NULL_CHILD);
            // assign the key
            m_key = key;
        }

        public Object getKey()
        {
            return m_key;
        }
        
        public void addParent(Node node)
        {
            if (m_parents == null && node != null)
                m_parents = new HashSet<Node>();
            // only add a child if it is not defined already
            if (m_parents.contains(node) == false)
                m_parents.add(node);
        }
        
        public Set<Node> getParents()
        {
            return m_parents;
        }

        public void addChild(Node node)
        {
            if (m_children == null && node != null)
                m_children = new HashSet<Node>();
            // only add a child if it is not defined already
            if (m_children.contains(node) == false)
                m_children.add(node);
        }

        public boolean removeParent(Node node)
        {
            if (node != null && m_parents != null)
            {
                return m_parents.remove(node);
            }
            else
            {
                return false;
            }
        }

        public boolean removeChild(Node node)
        {
            if (m_children != null && node != null)
                return m_children.remove(node);
            else
                return false;
        }

        public Set<Node> getChildren()
        {
            return m_children;
        }

        public int getChildrenSize()
        {
            return m_children != null ? m_children.size() : 0;
        }

        public int hashCode()
        {
            return m_key.hashCode();
        }

        public boolean equals(Object obj)
        {
            if (obj instanceof Node)
            {
                Node other = (Node) obj;
                return m_key.equals(other.m_key);
            }
            else
            {
                return false;
            }
        }
        
        public String toString()
        {
            return m_key.toString();
        }

        public Node clone()
        {
            Node newNode = null;
            
            try
            {
                newNode = new Node(m_key);
                
                if(m_parents != null)
                {
                    newNode.m_parents = new HashSet<Node>();
                    newNode.m_parents.addAll(m_parents);
                }
                
                if(getChildrenSize() > 0)
                {
                    Iterator<Node> children = m_children.iterator();
                    Node temp;

                    newNode.m_children = new HashSet<Node>();
                    while(children.hasNext())
                    {
                        temp = children.next();
                        newNode.m_children.add(temp.clone());
                    }
                }
            }
            catch (DependencyException e)
            {
                // a logger should be used here
                e.printStackTrace();
            }

            return newNode;
        }
    }
}

